# Vue_Express_Starter_App

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run Dev Server
```
npm run server
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
